<?php

namespace ManthanB\Base64images\ViewHelpers;

/* *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */

use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\AbstractFileFolder;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Fluid\Core\ViewHelper\Exception;
use TYPO3\CMS\Fluid\Core\ViewHelper\Facets\CompilableInterface;

class ImageUriViewHelper extends AbstractViewHelper implements CompilableInterface {

  /**
   * Initialize arguments
   */
  public function initializeArguments() {
    $this->registerArgument('src', 'string', 'src');
    $this->registerArgument('treatIdAsReference', 'bool', 'given src argument is a sys_file_reference record', false, false);
    $this->registerArgument('image', 'object', 'image');
    $this->registerArgument('crop', 'string|bool', 'overrule cropping of image (setting to FALSE disables the cropping set in FileReference)');
    $this->registerArgument('cropVariant', 'string',
      'select a cropping variant, in case multiple croppings have been specified or stored in FileReference', false, 'default');

    $this->registerArgument('width', 'string',
      'width of the image. This can be a numeric value representing the fixed width of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.');
    $this->registerArgument('height', 'string',
      'height of the image. This can be a numeric value representing the fixed height of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.');
    $this->registerArgument('minWidth', 'int', 'minimum width of the image');
    $this->registerArgument('minHeight', 'int', 'minimum height of the image');
    $this->registerArgument('maxWidth', 'int', 'maximum width of the image');
    $this->registerArgument('maxHeight', 'int', 'maximum height of the image');
    $this->registerArgument('absolute', 'bool', 'Force absolute URL', false, false);
    $this->registerArgument('convert', 'bool', 'Convert to base64', false, false);
  }

  /**
   * Resizes the image (if required) and returns its path. If the image was not resized, the path will be equal to $src
   *
   * @see https://docs.typo3.org/typo3cms/TyposcriptReference/ContentObjects/ImgResource/
   * @param string $src
   * @param FileInterface|AbstractFileFolder $image
   * @param string $width width of the image. This can be a numeric value representing the fixed width of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.
   * @param string $height height of the image. This can be a numeric value representing the fixed height of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.
   * @param int $minWidth minimum width of the image
   * @param int $minHeight minimum height of the image
   * @param int $maxWidth maximum width of the image
   * @param int $maxHeight maximum height of the image
   * @param int $convert convert image into base64
   * @param bool $treatIdAsReference given src argument is a sys_file_reference record
   * @param string|bool $crop overrule cropping of image (setting to FALSE disables the cropping set in FileReference)
   * @param bool $absolute Force absolute URL
   * @throws Exception
   * @return string path to the image
   */
  public function render() {
    extract($this->arguments);
    return self::renderStatic(
        array(
        'src'                => $src,
        'image'              => $image,
        'width'              => $width,
        'height'             => $height,
        'minWidth'           => $minWidth,
        'minHeight'          => $minHeight,
        'maxWidth'           => $maxWidth,
        'maxHeight'          => $maxHeight,
        'treatIdAsReference' => $treatIdAsReference,
        'crop'               => $crop,
        'absolute'           => $absolute,
        'convert'            => $convert,
        ), $this->buildRenderChildrenClosure(), $this->renderingContext
    );
  }

  /**
   * @param array $arguments
   * @param callable $renderChildrenClosure
   * @param RenderingContextInterface $renderingContext
   * @return string
   * @throws Exception
   */
  public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
    $src = $arguments['src'];
    $image = $arguments['image'];
    $treatIdAsReference = $arguments['treatIdAsReference'];
    $crop = $arguments['crop'];
    $absolute = $arguments['absolute'];

    if (is_null($src) && is_null($image) || !is_null($src) && !is_null($image)) {
      throw new Exception('You must either specify a string src or a File object.', 1382284105);
    }

    try {
      $imageService = self::getImageService();
      $image = $imageService->getImage($src, $image, $treatIdAsReference);

      if ($crop === null) {
        $crop = $image instanceof FileReference ? $image->getProperty('crop') : null;
      }

      $processingInstructions = array(
        'width'     => $arguments['width'],
        'height'    => $arguments['height'],
        'minWidth'  => $arguments['minWidth'],
        'minHeight' => $arguments['minHeight'],
        'maxWidth'  => $arguments['maxWidth'],
        'maxHeight' => $arguments['maxHeight'],
        'crop'      => $crop,
      );
      $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
      $imageUri = $imageService->getImageUri($processedImage, $absolute);

      if ($arguments['convert'] != false) {
        $root = explode("/", realpath($_SERVER['DOCUMENT_ROOT']));

        $root = implode("/", $root);
        $type = pathinfo($root . $imageUri, PATHINFO_EXTENSION);
        $data = file_get_contents($root . $imageUri);
        if ($data != '') {
          $imageUri = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
      }

      return $imageUri;
    } catch (ResourceDoesNotExistException $e) {
      // thrown if file does not exist
    } catch (\UnexpectedValueException $e) {
      // thrown if a file has been replaced with a folder
    } catch (\RuntimeException $e) {
      // RuntimeException thrown if a file is outside of a storage
    } catch (\InvalidArgumentException $e) {
      // thrown if file storage does not exist
    }
    return '';
  }

  /**
   * Return an instance of ImageService using object manager
   *
   * @return ImageService
   */
  protected static function getImageService() {
    /** @var ObjectManager $objectManager */
    $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
    return $objectManager->get(ImageService::class);
  }

}
